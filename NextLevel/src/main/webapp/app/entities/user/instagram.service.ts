import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApplicationConfigService } from 'app/core/config/application-config.service';


@Injectable({ providedIn: 'root' })
export class InstagramService {
  public resourceUrl = "https://graph.facebook.com/v3.2/17841456109243702?";
 

  constructor(
    private http: HttpClient, 
    private applicationConfigService: ApplicationConfigService,
  ) {}

  findByName(name: string): Observable<HttpResponse<any>> {
    return this.http.get<any>(this.resourceUrl+"fields=business_discovery.username("+name+"){followers_count,media_count,username}&access_token=EAALbOZAFPVQwBAKIIJkkKavX2XoZBujmIg8vH5yjPjwny5tZBweyVZClQRfyl78ZAIZA5ZApN1lsWadWqX3hTnYfVc7VmuTAtrkHU3xyelkTWpTjnLvlKXx0cQIgjPGrqiLHWqsD8YyzbW6zgO8ERijVpqgEWqfpTlvIxS48P7b8PTAZC0A2MEoz", { observe: 'response' });
  }
}
  